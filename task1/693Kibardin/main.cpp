#include "./common/Mesh.hpp"
#include "./common/Application.hpp"
#include "./common/ShaderProgram.hpp"
#include <vector>
#include <cmath>
#include <iostream>

// Draws Surface of Revolution (http://virtualmathmuseum.org/Surface/k1-sor/k1-sor.html)
class SurfaceOfRevolutionApplication :
        public Application {
public:
    void makeScene() override final {
        Application::makeScene();
        _cameraMover = std::make_shared<FreeCameraMover>();
        surfaceMesh_ = getSurfaceMesh_(detalization_);
        shader_ = std::make_shared<ShaderProgram>("693KibardinData1/shader.vert", "693KibardinData1/shader.frag");
    }

    void draw() override final {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);
        glViewport(0, 0, width, height);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        shader_->use();
        shader_->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        shader_->setMat4Uniform("projectionMatrix", _camera.projMatrix);
        shader_->setMat4Uniform("modelMatrix", surfaceMesh_->modelMatrix());
        surfaceMesh_->draw();
    }

    void handleKey(int key, int scancode, int action, int mods) override final {
        Application::handleKey(key, scancode, action, mods);
        if (action == GLFW_PRESS) {
            if (key == GLFW_KEY_MINUS) {
                --detalization_;
                surfaceMesh_ = getSurfaceMesh_(detalization_);
                draw();
            } else if (key == GLFW_KEY_EQUAL) {
                ++detalization_;
                surfaceMesh_ = getSurfaceMesh_(detalization_);
                draw();
            }
        }
    }

private:
    static MeshPtr getSurfaceMesh_(unsigned detalization) {
        std::vector<glm::vec3> vertices;
        std::vector<glm::vec3> normals;

        // std::cout << "min param value " << getMinParamValue_() << std::endl;
        std::cout << "detalization    " << detalization << std::endl;

        auto paramStep = (glm::pi<float>() - 2.0 * getMinParamValue_()) / detalization;
        auto angleStep = 2.0 * glm::pi<float>() / detalization;
        for (float param = getMinParamValue_();
             param < glm::pi<float>() - getMinParamValue_() - paramStep; param += paramStep) {
            auto radius = getRadius_(param);
            auto height = getHeight_(param);
            auto nextRadius = getRadius_(param + paramStep);
            auto nextHeight = getHeight_(param + paramStep);
            for (float angle = 0; angle < 2.0 * glm::pi<float>(); angle += angleStep) {
                auto nextAngle = angle + angleStep;

                auto currPoint = getPoint_(radius, angle, height);
                auto nextAnglePoint = getPoint_(radius, nextAngle, height);
                auto nextAngleAndParamPoint = getPoint_(nextRadius, nextAngle, nextHeight);
                auto nextParamPoint = getPoint_(nextRadius, angle, nextHeight);

                vertices.emplace_back(currPoint);
                vertices.emplace_back(nextAnglePoint);
                vertices.emplace_back(nextAngleAndParamPoint);
                for (unsigned i = 0; i < 3; ++i) {
                    normals.emplace_back(getNormal(currPoint, nextAnglePoint, nextAngleAndParamPoint));
                }

                vertices.emplace_back(nextAngleAndParamPoint);
                vertices.emplace_back(nextParamPoint);
                vertices.emplace_back(currPoint);
                for (unsigned i = 0; i < 3; ++i) {
                    normals.emplace_back(getNormal(nextAngleAndParamPoint, nextParamPoint, currPoint));
                }
            }
        }

        DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

        DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

        MeshPtr mesh = std::make_shared<Mesh>();
        mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
        mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
        mesh->setPrimitiveType(GL_TRIANGLES);
        mesh->setVertexCount(vertices.size());
        mesh->setModelMatrix(glm::mat4(1.0f));
        return mesh;
    }

    static float getRadius_(float param) {
        return SHAPE_PARAM_ * sin(param);
    }

    static float getRadiusDerivative_(float param) {
        return SHAPE_PARAM_ * cos(param);
    }

    class HeightDerivative_ {
    public:
        float operator()(float param) const {
            auto squaredDerivative = 1.0 - getRadiusDerivative_(param) * getRadiusDerivative_(param);
            // std::cout << "param in derivative " << param << std::endl;
            assert(squaredDerivative >= 0.0);
            return sqrt(squaredDerivative);
        }
    };

    static float getMinParamValue_() {
        float eps = 1e-5;
        return acos(1.0 / SHAPE_PARAM_) + eps;
    }

    static float getHeight_(float param) {
        return integrate_(HeightDerivative_(), getMinParamValue_(), param, HEIGHT_INTEGRAL_NUM_STEPS_);
    }

    static glm::vec3 getPoint_(float radius, float angle, float height) {
        return glm::vec3(radius * cos(angle), radius * sin(angle), height);
    }

    static glm::vec3 getNormal(glm::vec3 a, glm::vec3 b, glm::vec3 c) {
        auto v = b - a;
        auto u = c - a;
        glm::vec3 normal(
                v[1] * u[2] - v[2] * u[1],
                v[2] * u[0] - v[0] * u[2],
                v[0] * u[1] - v[1] * u[0]
        );
        if (a[2] < 0) {
            normal = -normal;
        }
        return normalize(normal);
    }

    template<class Functor>
    static double integrate_(Functor func, double from, double to, int num_steps) {
        assert(from <= to);
        double step = (to - from) / num_steps;  // width of each small rectangle
        double area = 0.0;  // signed area
        for (int i = 0; i < num_steps; i++) {
            area += func(from + (i + 0.5) * step) * step; // sum up each small rectangle
        }
        return area;
    }

private:
    MeshPtr surfaceMesh_;
    ShaderProgramPtr shader_;

    unsigned detalization_ = 30;
    constexpr static const float SHAPE_PARAM_ = 1.2;
    constexpr static const unsigned HEIGHT_INTEGRAL_NUM_STEPS_ = 100;
};

int main() {
    SurfaceOfRevolutionApplication app;
    app.start();
    return 0;
}
